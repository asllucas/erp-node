const express = require('express');
const cors = require('cors');

const app = express();

// ==> Rotas da API:
const index = require('./routes/index');
const clienteRoute = require('./routes/cliente-routes');
const produtoRoute = require('./routes/produto-routes');
const contaPagarRoute = require('./routes/conta-pagar-routes');
const contaReceberRoute = require('./routes/conta-receber-routes');
const insumoRoute = require('./routes/insumo-routes');
const fornecedorRoute = require('./routes/fornecedor-routes');
const funcionarioRoute = require('./routes/funcionario-routes');
const estoqueRoute = require('./routes/estoque-routes');
const vendaRoute = require('./routes/venda-routes');
const grupoInsumoRoute = require('./routes/grupo-insumo-routes');
const planoContaRoute = require('./routes/plano-conta-routes');
const usuarioRoute = require('./routes/usuario-routes');


app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.json({ type: 'application/vnd.api+json' }));
app.use(cors());

app.use(index);
app.use('/api/', clienteRoute);
app.use('/api/', produtoRoute);
app.use('/api/', contaPagarRoute);
app.use('/api/', contaReceberRoute);
app.use('/api/', insumoRoute);
app.use('/api/', fornecedorRoute);
app.use('/api/', funcionarioRoute);
app.use('/api/', estoqueRoute);
app.use('/api/', vendaRoute);
app.use('/api/', grupoInsumoRoute);
app.use('/api/', planoContaRoute);
app.use('/api/', usuarioRoute);

module.exports = app;