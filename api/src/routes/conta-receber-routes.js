const router = require('express-promise-router')();
const contaReceberController = require('../controllers/conta-receber-controller');

router.post('/contas-receber', contaReceberController.salvarContaReceber);
router.get('/contas-receber', contaReceberController.listarContasReceber);
router.get('/contas-receber/:id', contaReceberController.listarContaReceberPorId);
router.put('/contas-receber/:id', contaReceberController.atualizarContaReceber);
router.delete('/contas-receber/:id', contaReceberController.deletarContaReceber);

module.exports = router;