const router = require('express-promise-router')();
const usuarioController = require('../controllers/usuario-controller');
const autorizacao = require('../middlewares/auth')

//router.use(autorizacao);

router.post('/usuarios', usuarioController.salvarUsuario);
router.post('/autenticacao', usuarioController.login);
router.get('/usuarios', usuarioController.listarUsuarios);
router.get('/usuarios/:id', usuarioController.listarUsuarioPorId);
router.put('/usuarios/:id', usuarioController.atualizarUsuario);
router.delete('/usuarios/:id', usuarioController.deletarUsuario);

module.exports = router;