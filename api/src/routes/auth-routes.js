const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const db = require("../config/database");
const router = require('express-promise-router')();


// Sign-in
router.post("/login", (req, res, next) => {
    let getUsuario;
    console.log(req.body.email);
    db.query(`
        SELECT * FROM usuarios WHERE email = ${req.body.email}`
    ).then(user => {
       
        if(!user){
            return res.status(401).json({
                message: "Authentication failed"
            });
        }
       
        getUsuario = user;
        return bcrypt.compare(req.body.senha, user.senha);
    }).then(response => {
        
        if(!response){
            return res.status(401).json({
                message: "Authentication failed"
            });
        }
        let jwtToken = jwt.sign({
            email: getUsuario.email,
            id_usuario: getUsuario.id_usuario
        }, "longer-secret-is-better", {
            expiresIn: "1h"
        });
        res.status(200).json({
            token: jwtToken,
            expiresIn: 3600,
            id_usuario: getUsuario.id_usuario
        });
    }).catch(erro => {
        console.log(erro);
        return res.status(401).json({
            message: "Authentication failed"
        });
    })

})

module.exports = router;
