const router = require('express-promise-router')();
const funcionarioController = require('../controllers/funcionario-controller');

router.post('/funcionarios', funcionarioController.salvarFuncionario);
router.get('/funcionarios', funcionarioController.listarFuncionarios);
router.get('/funcionarios/:id', funcionarioController.listarFuncionarioPorId);
router.put('/funcionarios/:id', funcionarioController.atualizarFuncionario);
router.delete('/funcionarios/:id', funcionarioController.deletarFuncionario);

module.exports = router;