const router = require('express-promise-router')();
const vendaController = require('../controllers/venda-controller');

router.post('/vendas/:id', vendaController.salvarVenda);

module.exports = router;