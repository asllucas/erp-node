const router = require('express-promise-router')();
const grupoInsumoController = require('../controllers/grupo-insumo-controller');

router.post('/grupo-insumos', grupoInsumoController.salvarGrupoInsumo);
router.get('/grupo-insumos', grupoInsumoController.listarGrupoInsumos);
router.get('/grupo-insumos/:id', grupoInsumoController.listarGrupoInsumoPorId);
router.put('/grupo-insumos/:id', grupoInsumoController.atualizarGrupoInsumo);
router.delete('/grupo-insumos/:id', grupoInsumoController.deletarGrupoInsumo);

module.exports = router;