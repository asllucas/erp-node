const router = require('express-promise-router')();
const produtoController = require('../controllers/produto-controller');

router.post('/produtos', produtoController.salvarProduto);
router.get('/produtos', produtoController.listarProdutos);
router.get('/produtos/:id', produtoController.listarProdutoPorId);
router.put('/produtos/:id', produtoController.atualizarProduto);
router.delete('/produtos/:id', produtoController.deletarProduto);

module.exports = router;