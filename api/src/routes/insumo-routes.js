const router = require('express-promise-router')();
const insumoController = require('../controllers/insumo-controller');

router.post('/insumos', insumoController.salvarInsumo);
router.get('/insumos', insumoController.listarInsumos);
router.get('/insumos/:id', insumoController.listarInsumoPorId);
router.put('/insumos/:id', insumoController.atualizarInsumo);
router.delete('/insumos/:id', insumoController.deletarInsumo);

module.exports = router;