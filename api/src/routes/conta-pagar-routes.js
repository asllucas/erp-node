const router = require('express-promise-router')();
const contaPagarController = require('../controllers/conta-pagar-controller');

router.post('/contas-pagar', contaPagarController.salvarContaPagar);
router.get('/contas-pagar', contaPagarController.listarContasPagar);
router.get('/contas-pagar/:id', contaPagarController.listarContaPagarPorId);
router.put('/contas-pagar/:id', contaPagarController.atualizarContaPagar);
router.delete('/contas-pagar/:id', contaPagarController.deletarContaPagar);

module.exports = router;