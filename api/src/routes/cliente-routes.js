const router = require('express-promise-router')();
const clienteController = require('../controllers/cliente.controller');

router.post('/clientes', clienteController.salvarCliente);
router.get('/clientes', clienteController.listarClientes);
router.get('/clientes/:id', clienteController.listarClientePorId);
router.put('/clientes/:id', clienteController.atualizarCliente);
router.delete('/clientes/:id', clienteController.deletarCliente);

module.exports = router;