const router = require('express-promise-router')();
const fornecedorController = require('../controllers/fornecedor-controller');

router.post('/fornecedores', fornecedorController.salvarFornecedor);
router.get('/fornecedores', fornecedorController.listarFornecedores);
router.get('/fornecedores/:id', fornecedorController.listarFornecedorPorId);
router.put('/fornecedores/:id', fornecedorController.atualizarFornecedor);
router.delete('/fornecedores/:id', fornecedorController.deletarFornecedor);

module.exports = router;