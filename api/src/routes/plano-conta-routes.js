const router = require('express-promise-router')();
const planoContaController = require('../controllers/plano-conta-controller');

router.post('/plano-conta', planoContaController.salvarPlanoConta);
router.get('/plano-conta', planoContaController.listarPlanoConta);
router.get('/plano-conta/:id', planoContaController.listarPlanoContaPorId);
router.put('/plano-conta/:id', planoContaController.atualizarPlanoConta);
router.delete('/plano-conta/:id', planoContaController.deletarPlanoConta);

module.exports = router;
