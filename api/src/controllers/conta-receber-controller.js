const db = require("../config/database");
const moment = require("moment");

exports.salvarContaReceber = async (req, res) => {
    const { id_cliente, descricao, data_lancamento, data_vencimento, valor_conta, status, observacao } = req.body;
    const contasReceber = await db.query(
        `INSERT INTO contas_receber ( 
            id_cliente, 
            descricao, 
            data_lancamento, 
            data_vencimento, 
            valor_conta, 
            status,
            observacao  
            ) VALUES (?,?,?,?,?,?,?)`,
        [id_cliente, descricao, moment(data_lancamento).format('YYYY-MM-DD'), moment(data_vencimento).format('YYYY-MM-DD'), valor_conta, status, observacao]
    );

    res.status(201).send({
        message: "Conta salva com sucesso!",
        body: {
            contasReceber: { id_cliente, descricao, data_lancamento, data_vencimento, valor_conta, status }
        },
    });
};

exports.listarContasReceber = async (req, res) => {
    const contasReceber = await db.query(
        'SELECT * FROM contas_receber ORDER BY id_conta_receber ASC',
    );
    res.status(200).json(contasReceber[0]);
};

exports.listarContaReceberPorId = async (req, res) => {
    const contaReceberID = parseInt(req.params.id);
    const [contasReceber] = await db.query(
        'SELECT * FROM contas_receber WHERE id_conta_receber = ?',
        [contaReceberID],
    );
    res.status(200).json(contasReceber[0]);
};

exports.atualizarContaReceber = async (req, res) => {
    const contaReceberID = parseInt(req.params.id);
    const { descricao, data_lancamento, data_vencimento, valor_conta, status, observacao } = req.body;

    const contasReceber = await db.query(
        `UPDATE contas_receber SET 
        descricao = ?, 
        data_lancamento = ?, 
        data_vencimento = ?, 
        valor_conta = ?,
        status = ?,
        observacao = ?
       
        WHERE id_conta_receber = ?`,
        [descricao, moment(data_lancamento).format('YYYY-MM-DD'), moment(data_vencimento).format('YYYY-MM-DD'), valor_conta, status, observacao, contaReceberID]
    );

    res.status(200).send({ message: 'Conta atualizada com sucesso!' });
};

exports.deletarContaReceber = async (req, res) => {
    const contaReceberID = parseInt(req.params.id);
    await db.query('DELETE FROM contas_receber WHERE id_conta_receber = ?', [
        contaReceberID,
    ]);

    res.status(200).send({ message: 'Conta removida com sucesso!', contaReceberID });
};
