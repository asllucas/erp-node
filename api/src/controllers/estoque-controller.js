const db = require("../config/database");

exports.listarEstoqueProdutos = async (req, res, rows) => {
    const [response] = await db.query(
        `SELECT 
            prod.descricao, 
            est.quantidade, 
            est.valor_custo 
            
            FROM estoque est
            LEFT JOIN produtos prod ON prod.id_produto = est.id_produto`
    );
    res.status(200).json({ estoque: response });
};