const db = require("../config/database");

exports.salvarVenda = async (req, res) => {
    const {
        id_produto,
        quantidade,
        valor_unitario,
        id_item_venda,
        id_cliente,
        id_funcionario,
        data_venda,
        forma_pagamento,
        valor_total,
        observacao,
        data_vencimento
    } = req.body;

    //Insere na tabela de itens da venda
    const itemVenda = await db.query(
        "INSERT INTO itens_venda (id_produto, quantidade, valor_unitario) VALUES (?,?,?)",
        [id_produto, quantidade, valor_unitario]
    );

    // Recupera o Id o item da venda, id_produto e a quantidade da tabela itens de venda
    const itensVendaID = parseInt(req.params.id);
    const [itemsVenda] = await db.query('SELECT * FROM itens_venda WHERE id_item_venda = ?', [itensVendaID]);

    const itemId = itensVendaID;
    const prodId = itemsVenda[0].id_produto;
    const qtdeItemVenda = itemsVenda[0].quantidade;

    if (itemId) {
        const venda = await db.query(
            `INSERT INTO vendas (id_item_venda, id_cliente, id_funcionario, data_venda, forma_pagamento, valor_total, observacao, data_vencimento) VALUES (?,?,?,?,?,?,?,?)`,
            [id_item_venda, id_cliente, id_funcionario, data_venda, forma_pagamento, valor_total, observacao, data_vencimento]
        );
    }

    // Recupera a quantidade da tabela de produtos e realiza a subtração com a quantidade 
    // da tabela de itens da venda
    const produtoID = prodId
    const [responseProduto] = await db.query('SELECT * FROM produtos WHERE id_produto = ?', [produtoID]);
    const qtdeProduto = responseProduto[0].quantidade;
    const atualizaQuantidadeProduto = qtdeProduto - qtdeItemVenda;
    
    // Faz o update no campo de quantidade da tabela de produtos
    const produto = await db.query(
        `UPDATE produtos SET 
             quantidade = ${atualizaQuantidadeProduto}             
         
            WHERE id_produto = ${produtoID}`
    );
    res.status(201).send({
        message: "Venda efetuada com sucesso!",

    });
};