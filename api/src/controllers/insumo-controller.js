const db = require("../config/database");

exports.salvarInsumo = async (req, res) => {
    const { id_grupo_insumo, descricao, quantidade, estoque_minimo, estoque_maximo, valor_unitario, status } = req.body;
    const response = await db.query(
        "INSERT INTO insumos (id_grupo_insumo, descricao, quantidade, estoque_minimo, estoque_maximo, valor_unitario, status) VALUES (?,?,?,?,?,?,?)",
        [id_grupo_insumo, descricao, quantidade, estoque_minimo, estoque_maximo, valor_unitario, status]
    );

    res.status(201).send({
        message: "Insumo salvo com sucesso!",
        body: {
            insumo: { did_grupo_insumo, descricao, quantidade, estoque_minimo, estoque_maximo, valor_unitario, status }
        },
    });
};

exports.listarInsumos = async (req, res) => {
    const [response] = await db.query(
        'SELECT * FROM insumos ORDER BY descricao ASC',
    );
    res.status(200).json({ insumos: response });
};

exports.listarInsumoPorId = async (req, res) => {
    const insumoID = parseInt(req.params.id);
    const [response] = await db.query(
        'SELECT * FROM insumos WHERE id_insumo = ?',
        [insumoID],
    );
    res.status(200).json(response);
};

exports.atualizarInsumo = async (req, res) => {
    const insumoID = parseInt(req.params.id);
    const { id_grupo_insumo, descricao, quantidade, estoque_minimo, estoque_maximo, valor_unitario, status } = req.body;

    const response = await db.query(
        `UPDATE insumos SET 
        id_grupo_insumo = ?, 
        descricao = ?, 
        quantidade = ?, 
        estoque_minimo = ?, 
        estoque_maximo = ?, 
        valor_unitario = ?,
        status = ?
       
        WHERE id_insumo = ?`,
        [id_grupo_insumo, descricao, quantidade, estoque_minimo, estoque_maximo, valor_unitario, status, insumoID]
    );

    res.status(200).send({ message: 'Insumo atualizado com sucesso!' });
};

exports.deletarInsumo = async (req, res) => {
    const insumoID = parseInt(req.params.id);
    await db.query('DELETE FROM insumos WHERE id_insumo = ?', [
        insumoID,
    ]);

    res.status(200).send({ message: 'Insumo removido com sucesso!', insumoID });
};
