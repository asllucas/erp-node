const db = require("../config/database");

exports.salvarProduto = async (req, res) => {
    const { descricao, unidade, quantidade, valor_custo, valor_venda, status } = req.body;
    const produto = await db.query(
        `INSERT INTO produtos (
            descricao, 
            unidade, 
            quantidade, 
            valor_custo, 
            valor_venda, 
            status
            ) VALUES ($1, $2, $3, $4, $5, $6)`,
        [descricao, unidade, quantidade, valor_custo, valor_venda, status]
    );

    res.status(201).send({
        message: "Produto salvo com sucesso!",
        body: {
            produto: { descricao, unidade, quantidade, valor_custo, valor_venda, status }
        },
    });
};

exports.listarProdutos = async (req, res) => {
    const produtos = await db.query(
        'SELECT * FROM produtos ORDER BY id_produto ASC',
    );
     res.status(200).send(produtos.rows);
};

exports.listarProdutoPorId = async (req, res) => {
    const produtoID = parseInt(req.params.id);
    const produtos = await db.query(
        'SELECT * FROM produtos WHERE id_produto = $1',
        [produtoID],
    );
    res.status(200).send(produtos.rows[0]);
};

exports.atualizarProduto = async (req, res) => {
    const produtoID = parseInt(req.params.id);
    const { descricao, unidade, quantidade, valor_custo, valor_venda, status } = req.body;

    const response = await db.query(
        `UPDATE produtos SET 
        descricao = $1, 
        unidade = $2, 
        quantidade = $3, 
        valor_custo = $4, 
        valor_venda = $5, 
        status = $6
       
        WHERE id_produto = $7`,
        [descricao, unidade, quantidade, valor_custo, valor_venda, status, produtoID]
    );

    res.status(200).send({ message: 'Produto atualizado com sucesso!' });
};

exports.deletarProduto = async (req, res) => {
    const produtoID = parseInt(req.params.id);
    await db.query('DELETE FROM produtos WHERE id_produto = $1', [
        produtoID,
    ]);

    res.status(200).send({ message: 'Produto removido com sucesso!', produtoID });
};
