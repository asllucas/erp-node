const db = require("../config/database");

exports.salvarGrupoInsumo = async (req, res) => {
    const { descricao, status } = req.body;
    const response = await db.query(
        "INSERT INTO grupo_insumos (descricao, status) VALUES (?,?)",
        [descricao, status]
    );

    res.status(201).send({
        message: "Grupo insumo salvo com sucesso!",
        body: {
            grupoInsumo: { descricao, status }
        },
    });
};

exports.listarGrupoInsumos = async (req, res) => {
    const grupoInsumos = await db.query(
        'SELECT * FROM grupo_insumos ORDER BY id_grupo_insumo ASC',
    );
    res.status(200).json(grupoInsumos[0]);
};

exports.listarGrupoInsumoPorId = async (req, res) => {
    const grupoInsumoID = parseInt(req.params.id);
    const [grupoInsumos] = await db.query(
        'SELECT * FROM grupo_insumos WHERE id_grupo_insumo = ?',
        [grupoInsumoID],
    );
    res.status(200).json(grupoInsumos[0]);
};

exports.atualizarGrupoInsumo = async (req, res) => {
    const grupoInsumoID = parseInt(req.params.id);
    const { descricao, status } = req.body;

    const response = await db.query(
        `UPDATE grupo_insumos SET 
        descricao = ?, 
        status = ?
       
        WHERE id_grupo_insumo = ?`,
        [descricao, status, grupoInsumoID]
    );

    res.status(200).send({ message: 'Grupo insumo atualizado com sucesso!' });
};

exports.deletarGrupoInsumo = async (req, res) => {
    const grupoInsumoID = parseInt(req.params.id);
    await db.query('DELETE FROM grupo_insumos WHERE id_grupo_insumo = ?', [
        grupoInsumoID,
    ]);

    res.status(200).send({ message: 'Grupo insumo removido com sucesso!', grupoInsumoID });
};
