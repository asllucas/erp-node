const db = require("../config/database");
const moment = require("moment");

exports.salvarContaPagar = async (req, res) => {
    const { descricao, ocorrencia, id_plano_conta, forma_pagamento, valor_conta, data_lancamento, data_vencimento, pagamento, observacao } = req.body;
    const contasPagar = await db.query(
        `INSERT INTO contas_pagar (
                descricao, 
                ocorrencia,
                id_plano_conta,
                forma_pagamento, 
                valor_conta, 
                data_lancamento,
                data_vencimento,
                pagamento,
                observacao) 
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
        [descricao, ocorrencia, id_plano_conta, forma_pagamento, valor_conta, moment(data_lancamento).format('YYYY-MM-DD'), moment(data_vencimento).format('YYYY-MM-DD'), pagamento, observacao]
    );

    res.status(201).send({
        message: "Conta salva com sucesso!",
        body: {
            contasPagar: { descricao, ocorrencia, id_plano_conta, forma_pagamento, valor_conta, data_lancamento, data_vencimento, pagamento, observacao }
        },
    });
};

exports.listarContasPagar = async (req, res) => {
    const contasPagar = await db.query(
        'SELECT * FROM contas_pagar ORDER BY id_conta_pagar ASC'
    );
    res.status(200).json(contasPagar.rows);
};

exports.listarContaPagarPorId = async (req, res) => {
    const contaPagaID = parseInt(req.params.id);
    const contasPagar = await db.query(
        'SELECT * FROM contas_pagar WHERE id_conta_pagar = $1',
        [contaPagaID],
    );
    res.status(200).send(contasPagar.rows[0]);
};

exports.atualizarContaPagar = async (req, res) => {
    const contasPagarID = parseInt(req.params.id);
    const { descricao, ocorrencia, id_plano_conta, forma_pagamento, valor_conta, data_lancamento, data_vencimento, pagamento, observacao } = req.body;

    const response = await db.query(
        `UPDATE contas_pagar SET 
        descricao = $1, 
        ocorrencia = $2,
        id_plano_conta = $3,
        forma_pagamento $4, 
        valor_conta = $5,
        data_lancamento = $6, 
        data_vencimento = $7, 
        pagamento = $8,
        observacao = $9
       
        WHERE id_conta_pagar = $10`,
        [descricao, ocorrencia, id_plano_conta, forma_pagamento, valor_conta, moment(data_lancamento).format('YYYY-MM-DD'), moment(data_vencimento).format('YYYY-MM-DD'), pagamento, observacao, contasPagarID]
    );

    res.status(200).send({ message: 'Conta atualizada com sucesso!' });
};

exports.deletarContaPagar = async (req, res) => {
    const contasPagarID = parseInt(req.params.id);
    await db.query('DELETE FROM contas_pagar WHERE id_conta_pagar = $1', [
        contasPagarID,
    ]);

    res.status(200).send({ message: 'Conta removida com sucesso!', contasPagarID });
};
