const db = require("../config/database");

exports.salvarPlanoConta = async (req, res) => {
    const { conta, descricao } = req.body;
    const planoConta = await db.query(
        `INSERT INTO plano_contas (
                conta,    
                descricao) 
                VALUES ($1,$2)`,
        [conta, descricao]
    );
    res.status(201).send({
        message: "Plano de contas salvo com sucesso!",
        body: {
            planoConta: { conta, descricao }
        },
    });
};

exports.listarPlanoConta = async (req, res) => {
    const planoConta = await db.query(
        'SELECT * FROM plano_contas ORDER BY id_plano_conta ASC'
    );
    res.status(200).send(planoConta.rows);
};

exports.listarPlanoContaPorId = async (req, res) => {
    const planoContaID = parseInt(req.params.id);
    const planoConta = await db.query(
        'SELECT * FROM plano_contas WHERE id_plano_conta = $1',
        [planoContaID],
    );
    res.status(200).send(planoConta.rows[0]);
};

exports.atualizarPlanoConta = async (req, res) => {
    const planoContaID = parseInt(req.params.id);
    const { conta, descricao } = req.body;

    const response = await db.query(
        `UPDATE plano_contas SET
            conta = $1,
            descricao = $2
        WHERE id_plano_conta = $3`,
        [conta, descricao, planoContaID]
    );

    res.status(200).send({ message: 'Plano conta atualizado com sucesso!' });
};

exports.deletarPlanoConta = async (req, res) => {
    const planoContaID = parseInt(req.params.id);
    await db.query('DELETE FROM plano_contas WHERE id_plano_conta = $1', [
        planoContaID,
    ]);

    res.status(200).send({ message: 'Plano conta removido com sucesso!', planoContaID });
};