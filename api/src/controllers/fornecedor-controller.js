const db = require("../config/database");

exports.salvarFornecedor = async (req, res) => {
    const { nome, cnpj, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status } = req.body;
    const response = await db.query(
        `INSERT INTO fornecedores (
            nome, 
            cnpj, 
            endereco, 
            numero,
            bairro, 
            cep, 
            cidade, 
            uf, 
            telefone, 
            celular, 
            email, 
            status) 
            
            VALUES ($1,$2, $3. $4. $5. $6, $7, $8, $9, $10, $12)`,
        [nome, cnpj, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status]
    );

    res.status(201).send({
        message: "Fornecedor salvo com sucesso!",
        body: {
            fornecedor: { nome, cnpj, endereco, bairro, cep, cidade, uf, telefone, celular, email, status }
        },
    });
};

exports.listarFornecedores = async (req, res) => {
    const fornecedores = await db.query(
        'SELECT * FROM fornecedores ORDER BY id_fornecedor ASC',
    );
    res.status(200).json(fornecedores[0]);
};

exports.listarFornecedorPorId = async (req, res) => {
    const fornecedorID = parseInt(req.params.id);
    const [fornecedores] = await db.query(
        'SELECT * FROM fornecedores WHERE id_fornecedor = $1',
        [fornecedorID],
    );
    res.status(200).json(fornecedores[0]);
};

exports.atualizarFornecedor = async (req, res) => {
    const fornecedorID = parseInt(req.params.id);
    const { nome, cnpj, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status } = req.body;

    const fornecedores = await db.query(
        `UPDATE fornecedores SET 
        nome = $2, 
        cnpj = $3, 
        endereco = $4, 
        numero = $5,
        bairro = $6,
        cep = $7,
        cidade = $8,
        uf = $9,
        telefone = $10,
        celular = $11,
        email = $12,
        status = $13
        
        WHERE id_fornecedor = $1`,
        [nome, cnpj, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status, fornecedorID],
    );

    res.status(200).send({ message: 'Fornecedor atualizado com sucesso!' });
};

exports.deletarFornecedor = async (req, res) => {
    const fornecedorID = parseInt(req.params.id);
    await db.query('DELETE FROM fornecedores WHERE id_fornecedor = $1', [
        fornecedorID,
    ]);

    res.status(200).send({ message: 'Fornecedor removido com sucesso!', fornecedorID });
};