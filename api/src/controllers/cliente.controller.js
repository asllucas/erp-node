const db = require("../config/database");

exports.salvarCliente = async (req, res) => {
    const { nome, cpf, cnpj, tipo, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status } = req.body;
    const response = await db.query(
        `INSERT INTO clientes (
            nome, 
            cpf, 
            cnpj, 
            tipo, 
            endereco, 
            numero,
            bairro, 
            cep, 
            cidade, 
            uf, 
            telefone, 
            celular, 
            email, 
            status) 
            
            VALUES ($1, $2, $3, $4, $5, $6,$7, $8, $9,$10, $11, $12,$13, $14)`,
        [nome, cpf, cnpj, tipo, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status]
    );

    res.status(201).send({
        message: "Cliente salvo com sucesso!",
        body: {
            cliente: { nome, cpf, cnpj, tipo, endereco, bairro, cep, cidade, uf, telefone, celular, email, status }
        },
    });
};

exports.listarClientes = async (req, res) => {
    const clientes = await db.query(
        'SELECT * FROM clientes ORDER BY id_cliente ASC',
    );
    res.status(200).json(clientes[0]);
};

exports.listarClientePorId = async (req, res) => {
    const clienteID = parseInt(req.params.id);
    const [clientes] = await db.query(
        'SELECT * FROM clientes WHERE id_cliente = $1',
        [clienteID],
    );
    res.status(200).json(clientes[0]);
};

exports.atualizarCliente = async (req, res) => {
    const clienteID = parseInt(req.params.id);
    const { nome, cpf, cnpj, tipo, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status } = req.body;

    const response = await db.query(
        `UPDATE clientes SET 
        nome = $2, 
        cpf = $3, 
        cnpj = $4, 
        tipo = $5, 
        endereco = $6, 
        numero = $7,
        bairro = $8,
        cep = $9,
        cidade = $10,
        uf = $11,
        telefone = $12,
        celular = $13,
        email = $14,
        status = $15
        
        WHERE id_cliente = $1`,
        [nome, cpf, cnpj, tipo, endereco, numero, bairro, cep, cidade, uf, telefone, celular, email, status, clienteID],
    );

    res.status(200).send({ message: 'Cliente atualizado com sucesso!' });
};

exports.deletarCliente = async (req, res) => {
    const clienteID = parseInt(req.params.id);
    await db.query('DELETE FROM clientes WHERE id_cliente = $1', [
        clienteID,
    ]);

    res.status(200).send({ message: 'Cliente removido com sucesso!', clienteID });
};