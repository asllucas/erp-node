const db = require("../config/database");

exports.salvarFuncionario = async (req, res) => {
    const { nome, cpf, rg, tipo, sexo, endereco, numero, bairro, cep, cidade, uf, celular, email, status } = req.body;
    const response = await db.query(
        `INSERT INTO funcionarios (
            nome, 
            cpf, 
            rg,
            tipo, 
            sexo, 
            endereco, 
            numero,
            bairro, 
            cep, 
            cidade, 
            uf, 
            celular, 
            email, 
            status

            ) 

            VALUES ($1, $2, $3, $4, $5, $6,$7, $8, $9,$10, $11, $12,$13, $14)`,
        [nome, cpf, rg, tipo, sexo, endereco, numero, bairro, cep, cidade, uf, celular, email, status]
    );

    res.status(201).send({
        message: "Funcionario salvo com sucesso!",
        body: {
            funcionario: { nome, cpf, tipo, sexo, endereco, numero, bairro, cep, cidade, uf, celular, email, status }
        },
    });
};

exports.listarFuncionarios = async (req, res) => {
    const funcionarios = await db.query(
        'SELECT * FROM funcionarios ORDER BY id_funcionario ASC',
    );
    res.status(200).json(funcionarios[0]);
};

exports.listarFuncionarioPorId = async (req, res) => {
    const funcionarioID = parseInt(req.params.id);
    const [funcionarios] = await db.query(
        'SELECT * FROM funcionarios WHERE id_funcionario = $1',
        [funcionarioID],
    );
    res.status(200).json(funcionarios[0]);
};

exports.atualizarFuncionario = async (req, res) => {
    const funcionarioID = parseInt(req.params.id);
    const { nome, cpf, rg, tipo, sexo, endereco, numero, bairro, cep, cidade, uf, celular, email, status } = req.body;

    const response = await db.query(
        `UPDATE funcionarios SET 
        nome = $2, 
        cpf = $3, 
        rg = $4,
        tipo = $5,
        sexo = $6, 
        endereco = $7, 
        numero = $8,
        bairro = $9,
        cep = $10,
        cidade = $11,
        uf = $12,
        celular = $13,
        email = $14,
        status = $15
        
        WHERE id_funcionario = $1`,
        [nome, cpf, rg, tipo, sexo, endereco, numero, bairro, cep, cidade, uf, celular, email, status, funcionarioID],
    );

    res.status(200).send({ message: 'Funcionario atualizado com sucesso!' });
};

exports.deletarFuncionario = async (req, res) => {
    const funcionarioID = parseInt(req.params.id);
    await db.query('DELETE FROM funcionarios WHERE id_funcionario = $1', [
        funcionarioID,
    ]);

    res.status(200).send({ message: 'Funcionario removido com sucesso!', funcionarioID });
};