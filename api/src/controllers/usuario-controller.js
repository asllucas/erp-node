const db = require("../config/database");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const authConfig = require('../config/auth.json');

function generationToken(params = {}) {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400
    })
}

exports.salvarUsuario = async (req, res, err) => {
    const { nome, email, senha } = req.body;
    const senhaHash = await bcrypt.hash(senha, 10);

    const usuarioEmail = await db.query(
        `SELECT * FROM usuarios WHERE email = '${email}'`);

    if (usuarioEmail.rows.length > 0) {
        return res.status(400).send({ error: 'Usuario ja existe' });
    }

    const usuario = await db.query(
        `INSERT INTO usuarios (
            nome,
            email,
            senha
        ) 
            VALUES ($1, $2, $3)`,
        [nome, email, senhaHash]
    );

    res.status(201).send({
        message: "Usuario salvo com sucesso!",
        usuario : usuario.rows[0],
    });
};

exports.listarUsuarios = async (req, res) => {
    const usuarios = await db.query(
        'SELECT * FROM usuarios ORDER BY id_usuario ASC',
    );
    res.status(200).send(usuarios.rows);
};

exports.listarUsuarioPorId = async (req, res) => {
    const usuarioID = parseInt(req.params.id);
    const usuarios = await db.query(
        'SELECT * FROM usuarios WHERE id_usuario = $1',
        [usuarioID],
    );
    res.status(200).send(usuarios.rows[0]);
};

exports.atualizarUsuario = async (req, res) => {
    const usuarioID = parseInt(req.params.id);
    const { nome, email, senha } = req.body;
    const senhaHash = await bcrypt.hash(senha, 10);

    const response = await db.query(
        `UPDATE usuarios SET 
        nome = $1, 
        email = $2, 
        senha = $3
       
        WHERE id_usuario = $4`,
        [nome, email, senhaHash, usuarioID]
    );

    res.status(200).send({ message: 'Usuario atualizado com sucesso!' });
};

exports.deletarUsuario = async (req, res) => {
    const usuarioID = parseInt(req.params.id);
    await db.query('DELETE FROM usuarios WHERE id_usuario = $1', [
        usuarioID,
    ]);

    res.status(200).send({ message: 'Usuario removido com sucesso!', usuarioID });
};

exports.login = async (req, res) => {
    const { email, senha } = req.body;

    const usuario = await db.query(`
    SELECT * FROM usuarios WHERE email= '${email}'`);


    if (!usuario.rows[0]) {
        return res.status(400).send({ error: 'Usuário não encontrado' });
    }

    if (!await bcrypt.compare(senha, usuario.rows[0].senha)) {
        return res.status(400).send({ error: 'Senha Invalida' });
    }

    usuario.rows[0].senha = undefined;

    res.send({
        usuario: usuario.rows,
        token: generationToken({ id: usuario.rows[0].id_usuario })
    });
}
