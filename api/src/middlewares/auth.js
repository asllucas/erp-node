const jwt = require("jsonwebtoken");
const authConfig = require('../config/auth.json');

module.exports = (req, res, next) => {
    console.log(req.id);
    
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        return res.status(401).send({ erro: "Nenhum token fornecido" })
    }

    const parts = authHeader.split(' ');

    if (!parts === 2) {
        return res.status(401).send({ erro: "Falha no token" });
    }

    const { scheme, token } = parts;

    if (!/^Bearer$/i.test(scheme)) {
        return res.status(401).send({ erro: "Token nao formatado" })
    }

    
    jwt.verify(token, authConfig.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({ erro: "Token Invalido" })
        }


        req.id_usuario = decoded.id_usuario;
        return next();
    });


};